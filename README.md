# Visual Scripting for Unity

This repository contains the code for creating a topology of modules and defining their data flow.

## Contents

1. [Introduction](#introduction)
2. [Usage](#usage)


## Introduction
Modules inherit from the abstract "Module" class. 
There are two types of modules: data sources and filters.
Data sources send a raw data stream and filters can manipulate it - e.g scale the data.
Modules can be connected to each other to form a topology.
This is done visually on the editor screen.

The topology is compiled into two types of files - a data flow json that can be used for the project logic and a UI json.
The later represents the visual properties for loading the saved topology in the visual editor.

## Usage

1. Open lab manager project in unity.
2. Click Window->Lab Manager.
3. Select a module and drag it to the center.
4. Select another module and connect the first module's output to the second module's input.
5. When done press "Save Topology". Two json files will be created in unity persistent path (windows: user/AppData\LocalLow\DefaultCompany\lab_manager).
Use the data flow file to manage the lab data modules data flow.
6. To remove a module, right click and click "Remove Module".
7. To remove a link, click the small button on the link curved line.


![New Modue](lab_manager/Docs/new_module.PNG)

![Connect Modue](lab_manager/Docs/connect.PNG)



