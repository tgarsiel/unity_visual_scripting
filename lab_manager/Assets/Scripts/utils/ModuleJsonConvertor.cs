﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace LabManager
{
    public class ModuleJsonConvertor : JsonConverter
    {
       
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Module module = (Module)value;
            JToken t = JToken.FromObject(value);
            JObject o = (JObject)t;
            o.Add(new JProperty("Name", module.GetName()));
            o.WriteTo(writer);
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.StartObject:
                    JObject o = JObject.Load(reader);
                    return new BaseModule((string)o["Name"]);
                default:
                    return new BaseModule("");
            }
        }

    }

}


    