﻿namespace LabManager
{
    public class BaseModule : Module
    {
        private string name;

        public BaseModule(string name)
        {
            this.name = name;
        }

        public override int GetID()
        {
            throw new System.NotImplementedException();
        }

        public override string GetName()
        {
            return name;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

    }

}
