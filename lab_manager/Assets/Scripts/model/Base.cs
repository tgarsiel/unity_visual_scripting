﻿using System;
using System.Collections.Generic;

namespace LabManager
{
    public class Topology
    {
        public Dictionary<String, Module> Modules { get; set; }
    }
    public class Input
    {
        public String Source;
    }
    public class Output
    {
       public String Target;
    }
    public class DataStream
    {

    }
   
    public abstract class Module
    {
        public List<Input> Inputs { get; set; }
        public List<Output> Outputs { get; set; }

        public abstract int GetID();
        public abstract string GetName();
       
    }

    public abstract class DataSource :Module 
    {
        public abstract DataStream GetData();
    }
    public abstract class Filter :Module
    {
       public abstract DataStream FilterData(DataStream data);
    }
}
