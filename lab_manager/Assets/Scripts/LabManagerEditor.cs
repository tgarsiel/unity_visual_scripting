﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace LabManager
{
    public class LabManagerEditor : EditorWindow
    {
        private const string Title = "Lab Manager";
        private const string Header = "Build Toplogy";
        private const string DropBoxLabel = "Select Module";
        private List<Module> modules = new List<Module>()
        {
            new EEGDS(),new ECGDS(),new HRVDS(),new Scaler(),new Cleaner(),new Gui()
        };
        static TolpologyEditor tolpologyEditor = new TolpologyEditor();
        static EditorWindow mainWindow;
        private Vector2 offset;
        private Vector2 drag;
        private static DataLoader<TolpologyEditor> editorsLoader;
        private static DataLoader<Topology> topologyLoader;


        void Awake()
        {
            LoadTopology();
        }

        public static EditorWindow GetMainWindow()
        {
            return mainWindow;
        }
  
        [MenuItem("Window/Lab Manager")]
        public static void ShowWindow()
        {
            mainWindow = GetWindow<LabManagerEditor>(Title);
        }
        void OnGUI()
        {
            GUILayout.Label(Header, EditorStyles.boldLabel);
            DrawGrid(20, 0.2f, Color.gray);
            DrawGrid(100, 0.4f, Color.gray);
            GUIContent content = new GUIContent
            {
                text = DropBoxLabel
            };
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();

            DrawSaveButton();
            DrawModuleSelectionDropdown(content);
            tolpologyEditor.Draw();

            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }

        private void OnModuleAdded(object module)
        {
            ModuleEditor moduleEditor = new ModuleEditor((Module)module);
            tolpologyEditor.AddModule(moduleEditor,"", mainWindow.position.width);
        }

        private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
            int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            offset += drag * 0.5f;
            Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

            for (int i = 0; i < widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
            }

            for (int j = 0; j < heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
            }

            Handles.color = Color.white;
            Handles.EndGUI();
        }

        private static void DrawSaveButton()
        {
            Rect saveButton = new Rect(Screen.width - 110, 20, 100, 30);

            if (GUI.Button(saveButton, "Save Toplogy", Styles.SaveButton()))
            {
                Topology topology = BuildToplogy();
                editorsLoader.Save(tolpologyEditor);
                topologyLoader.Save(topology);
            }
        }

        private void DrawModuleSelectionDropdown(GUIContent content)
        {
            GUILayout.BeginArea(new Rect(10, 30, Screen.width - 150, 50));
            if (EditorGUILayout.DropdownButton(content, FocusType.Keyboard))
            {
                GenericMenu toolsMenu = new GenericMenu();
                foreach (Module module in modules)
                {
                    toolsMenu.AddItem(new GUIContent(module.GetName()), false, OnModuleAdded, module);
                    toolsMenu.AddSeparator("");
                }
                toolsMenu.DropDown(new Rect(0, 0, 100, 50));
                EditorGUIUtility.ExitGUI();
            }
            GUILayout.EndArea();
        }

        private void LoadTopology()
        {
            string editorDataPath = System.IO.Path.Combine(Application.persistentDataPath, "editors.json");
            string topologyDataPath = System.IO.Path.Combine(Application.persistentDataPath, "data_flow.json");
            editorsLoader = new DataLoader<TolpologyEditor>(editorDataPath);
            topologyLoader = new DataLoader<Topology>(topologyDataPath);

            TolpologyEditor savedTopology = editorsLoader.Load();
       
            foreach (KeyValuePair<string, ModuleEditor> entry in savedTopology.moduleEditors)
            {
                foreach (Module m in modules)
                {
                    if (m.GetName().Equals(entry.Value.module.GetName()))
                    {
                        entry.Value.module = m;
                    }
                }
                tolpologyEditor.AddModule(entry.Value,entry.Key,0);
            }

            foreach (KeyValuePair<string, LinkEditor> entry in savedTopology.linkEditors)
            {
                if(tolpologyEditor.moduleEditors.ContainsKey(entry.Value.InputKey) && tolpologyEditor.moduleEditors.ContainsKey(entry.Value.OutputKey))
                {
                    tolpologyEditor.AddLinkEditor(tolpologyEditor.moduleEditors[entry.Value.InputKey], tolpologyEditor.moduleEditors[entry.Value.OutputKey]);
                }
            }
        }


        private static Topology BuildToplogy()
        {
            Topology topology = new Topology();
            Dictionary<String, Module> modules = new Dictionary<String, Module>();
            foreach (KeyValuePair<string, ModuleEditor> entry in tolpologyEditor.moduleEditors)
            {
                modules[entry.Key] = entry.Value.module;
                List<Input> inputs = new List<Input>();
                List<Output> outputs = new List<Output>();
                foreach (KeyValuePair<string, LinkEditor> linkEntry in tolpologyEditor.linkEditors)
                {
                    if (linkEntry.Value.input.Key.Equals(entry.Key))
                    {
                        outputs.Add(new Output
                        {
                            Target = linkEntry.Value.input.Key
                        });
                    }
                    if (linkEntry.Value.output.Key.Equals(entry.Key))
                    {
                        inputs.Add(new Input
                        {
                            Source = linkEntry.Value.output.Key
                        });
                    }

                }
                modules[entry.Key].Outputs = outputs;
                modules[entry.Key].Inputs = inputs;
            }

            topology.Modules = modules;
            return topology;
        }
    }
}
    
