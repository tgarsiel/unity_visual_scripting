﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace LabManager
{
    public class Styles
    {
        public static GUIStyle ModuleStyle()
        {
            GUIStyle nodeStyle = new GUIStyle();
            nodeStyle.fontSize = 9;
            nodeStyle.normal.textColor = Color.white;
            nodeStyle.alignment = TextAnchor.MiddleCenter;
            nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node0.png") as Texture2D;
            return nodeStyle;
        }

        public static GUIStyle SelectedModuleStyle()
        {
            return ModuleStyle();
        }

        public static GUIStyle LeftLinkAnchor()
        {
            GUIStyle leftAnchorStyle = new GUIStyle();
            leftAnchorStyle.normal.background = EditorGUIUtility.Load("builtin skins/lightskin/images/btn left.png") as Texture2D;
            leftAnchorStyle.active.background = EditorGUIUtility.Load("builtin skins/lightskin/images/btn left on.png") as Texture2D;
            leftAnchorStyle.border = new RectOffset(4, 4, 12, 12);
            leftAnchorStyle.normal.textColor = Color.white;
            return leftAnchorStyle;
        }

        public static GUIStyle RighLinkAnchor()
        {
            GUIStyle rightAnchorStyle = new GUIStyle();
            rightAnchorStyle.normal.background = EditorGUIUtility.Load("builtin skins/lightskin/images/btn right.png") as Texture2D;
            rightAnchorStyle.active.background = EditorGUIUtility.Load("builtin skins/lightskin/images/btn right on.png") as Texture2D;
            rightAnchorStyle.border = new RectOffset(4, 4, 12, 12);
            rightAnchorStyle.normal.textColor = Color.white;
            return rightAnchorStyle;
        }

        public static GUIStyle SaveButton()
        {
            GUIStyle saveButtonStyle = new GUIStyle();
            saveButtonStyle.normal.textColor = Color.white;
            saveButtonStyle.alignment = TextAnchor.MiddleCenter;
            saveButtonStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
            saveButtonStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
            saveButtonStyle.border = new RectOffset(4, 4, 12, 12);
            return saveButtonStyle;
        }

    }
}


        