﻿using UnityEditor;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LabManager
{
    [System.Serializable]
    public class TolpologyEditor:Editor
    {
        public Dictionary<string,ModuleEditor> moduleEditors = new Dictionary<string, ModuleEditor>();
        public Dictionary<string, LinkEditor> linkEditors = new Dictionary<string, LinkEditor>();
        ModuleEditor selectedIn;
        ModuleEditor selectedOut;


        public void AddModule(ModuleEditor editor,string key, float position)
        {
            if (key.Equals(string.Empty))
            {
                key = ModuleKey(editor.module.GetID());
            }
            editor.OnInputClicked = ConnectInput;
            editor.OnOutputClicked = ConnectOutput;
            editor.OnRemove = RemoveNode;
            editor.Key = key;
            if(position > 1)
            {
                editor.Place(position);
            }
            if (!moduleEditors.ContainsKey(key))
            {
                moduleEditors.Add(key, editor);
            }
        }

        public void AddLinkEditor(ModuleEditor selectedIn,ModuleEditor selectedOut)
        {
            string key = LinkKey(selectedIn, selectedOut);
            LinkEditor linkEditor = new LinkEditor(selectedIn, selectedOut, this.RemoveLink);
            if(!linkEditors.ContainsKey(key))
            {
                linkEditors.Add(key, linkEditor);
            }
        }

        public void Connect(LinkEditor editor)
        {
            
        }
        public void Disconnect(LinkEditor editor)
        {

        }
        public void Draw()
        {
            EditorWindow mainWindow = LabManagerEditor.GetMainWindow();
            if (mainWindow == null)
            {
                return;
            }
            mainWindow.BeginWindows();
            DrawModules();
            DrawLinks();
            mainWindow.EndWindows();
            ProcessModuleEvents(Event.current);
        }

        private void DrawLinks()
        {
            try
            {
                foreach (KeyValuePair<string, LinkEditor> entry in linkEditors)
                {
                    entry.Value.Draw();
                }
            }
            catch (InvalidOperationException e)
            {

            }
        }

        private void DrawModules()
        {
            try
            {
                foreach (KeyValuePair<string, ModuleEditor> entry in moduleEditors)
                {
                    entry.Value.Draw();
                }
            }
            catch (InvalidOperationException e)
            {

            }
        }

        private void ProcessModuleEvents(Event ev)
        {
            try
            {
                foreach (KeyValuePair<string, ModuleEditor> entry in moduleEditors)
                {
                    bool guiChanged = entry.Value.ProcessEvents(ev);
                    if (guiChanged)
                    {
                        GUI.changed = true;
                    }
                }
            }
            catch (InvalidOperationException e)
            {

            }
        }

        private void ConnectInput(ModuleEditor editor)
        {
            selectedIn = editor;
            CreateLink();
        }

        private void ConnectOutput(ModuleEditor editor)
        {
            selectedOut = editor;
            CreateLink();
        }

        private void CreateLink()
        {
            if (selectedIn == null || selectedOut == null || selectedIn.Key.Equals(selectedOut.Key))
            {
                return;
            }
            AddLinkEditor(selectedIn,selectedOut);
            selectedIn = null;
            selectedOut = null;
        }


        public void RemoveLink(ModuleEditor inModule, ModuleEditor outModule)
        {
            if (inModule == null || outModule == null)
            {
                return;
            }

            linkEditors.Remove(LinkKey(inModule, outModule));
        }

        public void RemoveNode(ModuleEditor editor)
        {
            string keyToRemove = editor.Key;
            List<String> editorLinkKeys = new List<String>();
            foreach (KeyValuePair<string, LinkEditor> entry in linkEditors)
            {
                if(entry.Value.InputKey.Equals(keyToRemove) || entry.Value.OutputKey.Equals(keyToRemove))
                {
                    editorLinkKeys.Add(entry.Key);
                }
            }
            moduleEditors.Remove(keyToRemove);
            foreach(String key in editorLinkKeys)
            {
                linkEditors.Remove(key);
            }
            
        }

        private static string LinkKey(ModuleEditor inModule, ModuleEditor outModule)
        {
            return string.Format("link{0}{1}", inModule.Key, outModule.Key);
        }


        private string ModuleKey(int v)
        {
            return String.Format("module_{0}_{1}", v,moduleEditors.Count);
        }
    }
}   
