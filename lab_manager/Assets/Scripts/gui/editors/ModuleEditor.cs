﻿using UnityEngine;
using System;
using Newtonsoft.Json;
using UnityEditor;

namespace LabManager
{
    [System.Serializable]
    public class ModuleEditor : Editor
    {
        public string Key { get; set; }
        [JsonConverter(typeof(ModuleJsonConvertor))]
        public Module module { get; set; }
        [JsonIgnore]
        public Action<ModuleEditor> OnInputClicked { get; set; }
        [JsonIgnore]
        public Action<ModuleEditor> OnOutputClicked { get; set; }
        [JsonIgnore]
        public Action<ModuleEditor> OnRemove { get; set; }
        public Rect Rect { get; set; }
        private bool isDragged;
        private bool isSelected;
        [JsonIgnore]
        private GUIStyle nodeStyle = Styles.ModuleStyle();
        private Texture2D inputImage;
        private Texture2D outputImage;

        public ModuleEditor()
        {
            Rect = new Rect(50, 50, 100, 65);
        }

        public ModuleEditor(Module module):this()
        {
            this.module = module;
        }

        public void Place(float position)
        {
            Rect = new Rect(position - Rect.width - 10, Rect.y, Rect.width, Rect.height);
        }

        public void DrawWindow(int id)
        {
            GUI.DragWindow(new Rect(0, 0, 10000, 10000));
        }

        public void Draw()
        {
            Rect innerBox = new Rect(Rect.x, Rect.y, Rect.width, Rect.height);
            GUI.Box(innerBox, this.module.GetName(), nodeStyle);
            DrawLinkAnchors(innerBox);
        }

        public void Drag(Vector2 delta)
        {
            Rect = new Rect(Rect.x + delta.x, Rect.y + delta.y, Rect.width, Rect.height);
        }

        private void DrawLinkAnchors(Rect innerBox)
        {
            LoadImages();
            DrawAnchor(Rect.x - 7, Styles.LeftLinkAnchor(), OnInputClicked, inputImage);
            DrawAnchor(Rect.x + innerBox.width - 14, Styles.RighLinkAnchor(), OnOutputClicked,outputImage);
        }


        private void DrawAnchor(float xPos, GUIStyle style, Action<ModuleEditor> onAnchorClicked,Texture2D image)
        {

            Rect linkPoint = new Rect(xPos, Rect.y + Rect.height/2 - 5, 20, 17);

            if (GUI.Button(linkPoint, image, style))
            {
                onAnchorClicked(this);
            }

        }

        public bool ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        if (Rect.Contains(e.mousePosition))
                        {
                            isDragged = true;
                            GUI.changed = true;
                            isSelected = true;
                            nodeStyle = Styles.SelectedModuleStyle();
                        }
                        else
                        {
                            GUI.changed = true;
                            isSelected = false;
                            nodeStyle = Styles.ModuleStyle();
                        }
                    }

                    if (e.button == 1 && Rect.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }
                    break;

                case EventType.MouseUp:
                    isDragged = false;
                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }
                    break;
            }

            return false;
        }

        private void ProcessContextMenu()
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Remove node"), false, OnClickRemoveNode);
            genericMenu.ShowAsContext();
        }

        private void OnClickRemoveNode()
        {
            if (OnRemove != null)
            {
                OnRemove(this);
            }
        }

    
        private void LoadImages()
        {
            if (inputImage == null)
            {
                inputImage = (Texture2D)EditorGUIUtility.Load("input16_2.png");
                outputImage = (Texture2D)EditorGUIUtility.Load("output16_2.png");
            }
        }
    }
}