﻿using UnityEngine;
using UnityEditor;
using LabManager;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;

namespace LabManager
{
    [System.Serializable]
    public class LinkEditor : Editor
    {
        public string InputKey { get; set; }
        public string OutputKey { get; set; }
        [JsonIgnore]
        public ModuleEditor input { get; set; }
        [JsonIgnore]
        public ModuleEditor output { get; set; }

        private Action<ModuleEditor,ModuleEditor> onRemove;

        public LinkEditor()
        {

        }
        public LinkEditor(ModuleEditor input, ModuleEditor output, Action<ModuleEditor, ModuleEditor> onRemove)
        {
            this.input = input;
            this.output = output;
            this.InputKey = input.Key;
            this.OutputKey = output.Key;
            this.onRemove = onRemove;
        }

        public void Draw()
        {
            if(input == null || output == null)
            {
                return;
            }
            Handles.DrawBezier(
             input.Rect.center + Vector2.left * (output.Rect.width / 2 + 10) + Vector2.up * 5,
             output.Rect.center + Vector2.right * (output.Rect.width / 2 + 10) + Vector2.up * 5,
             input.Rect.center + Vector2.left * (output.Rect.width / 2 + 10) * 2,
             output.Rect.center + Vector2.right * (output.Rect.width / 2 + 10) * 2,
             Color.white,
             null,
             4f
         );
            if (Handles.Button((input.Rect.center + output.Rect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleCap))
            {
                onRemove(input, output);
            }
        }
    }
}